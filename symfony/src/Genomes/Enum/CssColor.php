<?php

declare(strict_types=1);

namespace App\Genomes\Enum;

enum CssColor : string
{
    case BLUE = 'rgb( 0, 0, 128)';
    case GREEN = 'rgb( 0, 128, 0)';
    case WHITE = 'rgb( 255, 255, 255)';
    case RED = 'rgb( 255, 0, 0)';
}
