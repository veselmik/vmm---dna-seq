<?php

namespace App\Genomes\Enum;

enum Sequence: string
{
    case SEQUENCE_A = 'sequence_a';
    case SEQUENCE_B = 'sequence_b';

    public function getRedisKeyFromIndex(int $index): string
    {
        return md5($this->value . $index);
    }
}