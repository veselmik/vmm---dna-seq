<?php

namespace App\Genomes\Enum;

enum Nucleobase : string
{
    case ADENINE = 'adenine';
    case CYTOSINE = 'cytosine';
    case GUANINE = 'guanine';
    case THYMINE = 'thymine';
}