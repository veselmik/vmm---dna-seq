<?php

declare(strict_types=1);

namespace App\Genomes\DTO;

class MappedPoint
{
    public function __construct(public int $datasetIndex, public int $index)
    {
    }
}