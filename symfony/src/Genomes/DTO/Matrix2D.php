<?php

declare(strict_types=1);

namespace App\Genomes\DTO;

class Matrix2D
{
    /** @var list<list<float|int>> */
    private array $data = [];

    public function getAt(int $i, int $j): float|int
    {
        if (!isset($this->data[$i][$j])) {
            return INF;
        }

        return $this->data[$i][$j];
    }

    public function setAt(int $i, int $j, float|int $content): void
    {
        $this->data[$i][$j] = $content;
    }
}