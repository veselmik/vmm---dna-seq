<?php

declare(strict_types=1);

namespace App\Genomes\DTO;

use App\Genomes\Enum\CssColor;

class Dataset
{
    public int $pointRadius = 6;
    public int $pointHoverRadius = 6;
    public int $pointHoverBorderWidth = 2;

    /** @param list<int|float> $data */
    public function __construct(
        public string $label,
        public CssColor $backgroundColor,
        public CssColor $borderColor,
        public CssColor $pointHoverBackgroundColor,
        public CssColor $pointHoverBorderColor,
        public array $data
    ) {
    }
}
