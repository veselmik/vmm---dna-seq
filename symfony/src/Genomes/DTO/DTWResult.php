<?php

declare(strict_types=1);

namespace App\Genomes\DTO;

class DTWResult
{
     /** @param list<MatrixPoint> $path */
    public function __construct(public int|float $cost, public array $path)
    {
    }
}
