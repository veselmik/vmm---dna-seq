<?php

declare(strict_types=1);

namespace App\Genomes\DTO;

class MatrixPoint
{
    public function __construct(public readonly int $row, public readonly int $column)
    {
    }
}
