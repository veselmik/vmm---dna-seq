<?php

declare(strict_types=1);

namespace App\Genomes\DTO;

use App\Genomes\Enum\CssColor;

use function array_map;
use function array_merge;
use function str_split;

class Genome
{
    private string $genomeString = '';

    private string $label = '';

    public function __construct(private NucleobaseConfig|null $nucleobaseConfig = null)
    {
    }

    public function setLabel(string $label): self
    {
        $this->label = $label;

        return $this;
    }

    public function getLabel(): string
    {
        return $this->label;
    }

    public function addGenomeString(string $genomeString): self
    {
        $this->genomeString .= $genomeString;

        return $this;
    }


    /** @return list<float|int> */
    public function getMappedValuesUsingInnerConfig(bool $addZeroIndex): array
    {
        $mappedValues = array_map(
            fn(string $char) => $this->nucleobaseConfig->weighs[$char],
            str_split($this->genomeString)
        );

        if ($addZeroIndex) {
            $shiftArray = [0];
            $mappedValues = array_merge($shiftArray, $mappedValues);
        }

        return $mappedValues;
    }

    /** @return list<float|int> */
    public function getMappedValues(NucleobaseConfig $nucleobaseConfig, bool $addZeroIndex): array
    {
        $mappedValues = array_map(
            fn(string $char) => $nucleobaseConfig->weighs[$char],
            str_split($this->genomeString)
        );

        if ($addZeroIndex) {
            $shiftArray = [0];
            $mappedValues = array_merge($shiftArray, $mappedValues);
        }

        return $mappedValues;
    }

    public function getDataset(NucleobaseConfig $nucleobaseConfig, CssColor $color, ?string $label = null): Dataset
    {
        return new Dataset(
            $label ?? $this->label,
            $color,
            $color,
            CssColor::WHITE,
            CssColor::RED,
            $this->getMappedValues($nucleobaseConfig, false)
        );
    }

    public function getRedisKey(): string
    {
        return md5($this->label);
    }

    public function getGenomeString(): string
    {
        return $this->genomeString;
    }
}
