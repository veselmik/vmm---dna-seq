<?php

declare(strict_types=1);

namespace App\Genomes\DTO;

class NucleobaseConfig
{
    private const ADENINE = 'A';
    private const CYTOSINE = 'C';
    private const GUANINE = 'G';
    private const THYMINE = 'T';

    /** @var array<string, float> */
    public readonly array $weighs;

    public function __construct(float $adenine, float $cytosine, float $guanine, float $thymine)
    {
        $this->weighs = [
            self::ADENINE => $adenine,
            self::CYTOSINE => $cytosine,
            self::GUANINE => $guanine,
            self::THYMINE => $thymine,
        ];
    }

    public function adenine() : float
    {
        return $this->weighs[self::ADENINE];
    }

    public function cytosine() : float
    {
        return $this->weighs[self::CYTOSINE];
    }

    public function guanine() : float
    {
        return $this->weighs[self::GUANINE];
    }

    public function thymine() : float
    {
        return $this->weighs[self::THYMINE];
    }
}
