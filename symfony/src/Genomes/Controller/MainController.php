<?php

declare(strict_types=1);

namespace App\Genomes\Controller;

use App\Genomes\DTO\Genome;
use App\Genomes\DTO\MappedPoint;
use App\Genomes\DTO\NucleobaseConfig;
use App\Genomes\Enum\CssColor;
use App\Genomes\Enum\Nucleobase;
use App\Genomes\Enum\Sequence;
use App\Genomes\Service\DTWCalculator;
use App\Genomes\Service\FastaParser;
use Predis\Client;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\File;

use function array_map;
use function assert;
use function count;
use function file_get_contents;
use function json_decode;
use function range;

class MainController extends AbstractController
{
    private const REDIS_GENOME_LABELS_KEY = 'genome_labels';
    private const REDIS_CHART_CONFIG_KEY = 'chart_config';

    public function __construct(
        private Client        $redisMain,
        private Client        $redisMapping,
        private DTWCalculator $DTWCalculator
    )
    {
    }

    #[Route('/chart', name: 'sequences.chart')]
    public function chart(): Response
    {
        $genomeLabelArray = $this->redisMain->get(self::REDIS_GENOME_LABELS_KEY);
        $chartConfig = $this->redisMain->get(self::REDIS_CHART_CONFIG_KEY);
        if ($genomeLabelArray === null || $chartConfig === null) {
            return $this->redirect('/');
        }
        $chartConfig = json_decode($chartConfig, true);
        $genomeLabelArray = json_decode($genomeLabelArray, true);
        $nucleoBaseConfig = new NucleobaseConfig(
            $chartConfig[Nucleobase::ADENINE->value],
            $chartConfig[Nucleobase::CYTOSINE->value],
            $chartConfig[Nucleobase::GUANINE->value],
            $chartConfig[Nucleobase::THYMINE->value]
        );

        $genomeA = (new Genome($nucleoBaseConfig))->setLabel($genomeLabelArray[$chartConfig['genomeA']]);
        $genomeA->addGenomeString($this->redisMain->get($genomeA->getRedisKey()));

        $start = microtime(true);
        $this->redisMain->set('totalDtw', 0);
        $alignCosts = [];
        foreach ($genomeLabelArray as $genome) {
            $genomeB = (new Genome($nucleoBaseConfig))->setLabel($genome);
            $genomeB->addGenomeString($this->redisMain->get($genomeB->getRedisKey()));
            $alignCost = $this->DTWCalculator->calculate(
                $genomeA,
                $genomeB,
                $chartConfig['constrained'] && is_numeric($chartConfig['bandwidth']),
                $chartConfig['bandwidth']
            );
            $alignCosts[$genomeB->getLabel()] = $alignCost;
        }
        $totalTime = microtime(true) - $start;

        asort($alignCosts);
        return $this->render(
            'chart/index.html.twig',
            [
                'currentGenomeLabel' => $genomeA->getLabel(),
                'nucleobases' => $nucleoBaseConfig,
                'totalTime' => number_format($totalTime, 3),
                'totalDtwCycles' => number_format((int)$this->redisMain->get('totalDtw')),
                'alignCosts' => $alignCosts
            ]
        );
    }

    #[Route('/', name: 'sequences.index')]
    public function index(Request $request): Response
    {
        $this->redisMain->flushdb();
        $this->redisMapping->flushdb();
        $form = $this->buildForm();
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $file = $form->get('file')->getData();
            assert($file instanceof UploadedFile);

            $genomes = FastaParser::parseFfn(file_get_contents($file->getPathname()));
            $genomeLabelArray = [];
            foreach ($genomes as $genome) {
                $genomeLabelArray[] = $genome->getLabel();
                $this->redisMain->set($genome->getRedisKey(), $genome->getGenomeString());
            }
            $this->redisMain->set(self::REDIS_GENOME_LABELS_KEY, json_encode($genomeLabelArray));

            return $this->redirectToRoute('sequences.config');
        }

        return $this->renderForm(
            'index.html.twig',
            ['form' => $this->buildForm()]
        );
    }

    #[Route('/config', name: 'sequences.config')]
    public function chooseGenomesAndConfig(Request $request): Response
    {
        $genomeLabelArray = $this->redisMain->get(self::REDIS_GENOME_LABELS_KEY);
        if ($genomeLabelArray === null) {
            return $this->redirect('/');
        }
        $genomeLabelArray = json_decode($genomeLabelArray, true);
        $existingConfig = $this->redisMain->get(self::REDIS_CHART_CONFIG_KEY);
        if ($existingConfig !== null) {
            $this->redisMapping->flushdb();
            $existingConfig = json_decode($existingConfig, true);
        }

        $form = $this->buildConfigForm(array_flip($genomeLabelArray));
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->redisMain->set(self::REDIS_CHART_CONFIG_KEY, json_encode($form->getData()));
            return $this->redirectToRoute('sequences.chart');
        }

        return $this->renderForm(
            'config/index.html.twig',
            ['form' => $form, 'existingConfig' => $existingConfig]
        );
    }

    #[Route('/chart-data', name: 'sequences.chart_data', methods: [Request::METHOD_POST])]
    public function getChartData(Request $request): JsonResponse
    {
        $chartConfig = json_decode($this->redisMain->get(self::REDIS_CHART_CONFIG_KEY), true);
        $nucleoBaseConfig = new NucleobaseConfig(
            $chartConfig[Nucleobase::ADENINE->value],
            $chartConfig[Nucleobase::CYTOSINE->value],
            $chartConfig[Nucleobase::GUANINE->value],
            $chartConfig[Nucleobase::THYMINE->value]
        );

        $genomeA = (new Genome())->setLabel($request->get('chartA'));
        $genomeA->addGenomeString($this->redisMain->get($genomeA->getRedisKey()));
        $genomeB = (new Genome())->setLabel($request->get('chartB'));
        $genomeB->addGenomeString($this->redisMain->get($genomeB->getRedisKey()));

        $datasetA = $genomeA->getDataset($nucleoBaseConfig, CssColor::BLUE);
        $datasetB = $genomeB->getDataset($nucleoBaseConfig, CssColor::GREEN);

        return new JsonResponse([
            'datasetA' => $datasetA,
            'datasetB' => $datasetB,
            'labelsA' => range(1, count($datasetA->data)),
            'labelsB' => range(1, count($datasetB->data))
        ]);
    }

    #[Route('/mapped-points', name: 'sequences.mapped_points', methods: [Request::METHOD_POST])]
    public function getMappedPoints(Request $request): JsonResponse
    {
        $params = json_decode($request->getContent(), true);
        /** index in chart starts at 0, mapping in redis at 1 */
        $index = $params['index'] + 1;
        $genomeA = (new Genome())->setLabel($params['chartA']);
        $genomeB = (new Genome())->setLabel($params['chartB']);
        $response = array_map(
        /** index in chart starts at 0, mapping in redis at 1 */
            static fn(int $point): MappedPoint => new MappedPoint(0, $point - 1),
            json_decode($this->redisMain->get($genomeA->getRedisKey() . $genomeB->getRedisKey() . $index))
        );

        return new JsonResponse(['activePoints' => $response]);
    }

    private function buildForm(): FormInterface
    {
        return $this->createFormBuilder()
            ->add('file', FileType::class, [
                'label' => 'DNA sequence file (.ffn): ',
                'mapped' => false,
                'required' => true,
                'constraints' => [
                    new File(['maxSize' => '4096k']),
                ],
            ])
            ->add('submit', SubmitType::class)
            ->getForm();
    }

    private function buildConfigForm(array $genomeOptionArray): FormInterface
    {
        return $this->createFormBuilder()
            ->add(Nucleobase::ADENINE->value, NumberType::class, [
                'label' => ucfirst(Nucleobase::ADENINE->value),
                'html5' => true
            ])
            ->add(Nucleobase::CYTOSINE->value, NumberType::class, [
                'label' => ucfirst(Nucleobase::CYTOSINE->value),
                'html5' => true
            ])
            ->add(Nucleobase::GUANINE->value, NumberType::class, [
                'label' => ucfirst(Nucleobase::GUANINE->value),
                'html5' => true
            ])
            ->add(Nucleobase::THYMINE->value, NumberType::class, [
                'label' => ucfirst(Nucleobase::THYMINE->value),
                'html5' => true
            ])
            ->add('genomeA', ChoiceType::class, [
                'choices' => $genomeOptionArray
            ])
            ->add('constrained', CheckboxType::class, [
                'label' => 'Sakoe-Chiba band',
                'required' => false,
            ])
            ->add('bandwidth', NumberType::class, [
                'label' => 'Sakoe-Chiba band width',
                'html5' => true
            ])
            ->add('submit', SubmitType::class)
            ->getForm();
    }
}
