<?php

declare(strict_types=1);

namespace App\Genomes\Service;

use App\Genomes\DTO\Genome;

use Predis\Client;
use function array_map;
use function array_shift;
use function explode;
use function preg_match;

class FastaParser
{
    /** @return array<Genome> */
    public static function parseFfn(string $file) : array
    {
        $genomeStrings = explode('>', $file);
        array_shift($genomeStrings);

        return array_map(
            static function (string $genomeInfo) : Genome {
                $genome = new Genome();
                foreach (explode("\n", $genomeInfo) as $genomeInfoLine) {
                    if (! preg_match('/^[ATGC]+$/', $genomeInfoLine) && $genomeInfoLine !== '') {
                        $genome->setLabel($genomeInfoLine);

                        continue;
                    }

                    $genome->addGenomeString($genomeInfoLine);
                }

                return $genome;
            },
            $genomeStrings
        );
    }
}
