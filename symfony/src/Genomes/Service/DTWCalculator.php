<?php

declare(strict_types=1);

namespace App\Genomes\Service;

use App\Genomes\DTO\Genome;
use App\Genomes\DTO\Matrix2D;
use App\Genomes\DTO\MatrixPoint;
use Predis\Client;

use function abs;
use function count;
use function json_decode;
use function json_encode;
use function min;

class DTWCalculator
{
    /** @var list<int|float> */
    private array $sequenceA;

    private int $sequenceALength;

    /** @var list<int|float> */
    private array $sequenceB;

    private int $sequenceBLength;

    /** @var list<list<MatrixPoint>> */
    private array $parentMatrix = [];

    public function __construct(private Client $redis, private Matrix2D $matrix = new Matrix2D())
    {
    }

    public function calculate(Genome $genomeA, Genome $genomeB, bool $constrainedBand, int|null $bandWidth = null): float|int
    {
        $totalDtwLoopCount = 0;

        $this->setVariables(
            $genomeA->getMappedValuesUsingInnerConfig(true),
            $genomeB->getMappedValuesUsingInnerConfig(true)
        );

        $this->matrix->setAt(0, 0, 0);

        $sequenceRatio = $this->sequenceBLength / $this->sequenceALength;
        for ($i = 1; $i < $this->sequenceALength; $i++) {
            if ($constrainedBand) {
                $j = max(1, (int)round($sequenceRatio * $i) - $bandWidth);
                $maxJ = min($this->sequenceBLength - 1, (int)round($sequenceRatio * $i) + $bandWidth);
            } else {
                $j = 1;
                $maxJ = $this->sequenceBLength - 1;
            }
            for (; $j <= $maxJ; $j++) {
                $cost = abs($this->sequenceA[$i] - $this->sequenceB[$j]);
                $this->matrix->setAt($i, $j, $cost + $this->minFromPreviousCorner($i, $j));
                $totalDtwLoopCount++;
            }
        }

        $this->saveSequenceMapping($genomeA, $genomeB);

        $this->redis->set('totalDtw', (int)$this->redis->get('totalDtw') + $totalDtwLoopCount);

        $alignCost = $this->matrix->getAt($this->sequenceALength - 1, $this->sequenceBLength - 1);
        $this->redis->set('alignCost', $alignCost);

        return $alignCost;
    }

    private function minFromPreviousCorner(int $i, int $j): float|int
    {
        $min = min($this->matrix->getAt($i - 1, $j), $this->matrix->getAt($i, $j - 1), $this->matrix->getAt($i - 1, $j - 1));
        if ($min === $this->matrix->getAt($i - 1, $j - 1)) {
            $minPoint = new MatrixPoint($i - 1, $j - 1);
        } elseif ($min === $this->matrix->getAt($i, $j - 1)) {
            $minPoint = new MatrixPoint($i, $j - 1);
        } else {
            $minPoint = new MatrixPoint($i - 1, $j);
        }

        $this->parentMatrix[$i][$j] = $minPoint;

        return $min;
    }

    private function saveSequenceMapping(Genome $genomeA, Genome $genomeB): void
    {
        $currentPoint = new MatrixPoint($this->sequenceALength - 1, $this->sequenceBLength - 1);
        while (true) {
            if (!isset($this->parentMatrix[$currentPoint->row][$currentPoint->column])) {
                break;
            }

            $parentPoint = $this->parentMatrix[$currentPoint->row][$currentPoint->column];

            $seqARedisKey = $genomeA->getRedisKey() . $genomeB->getRedisKey() . $currentPoint->row;
            $seqBRedisKey = $genomeB->getRedisKey() . $genomeA->getRedisKey() . $currentPoint->column;
            $previousContentA = json_decode($this->redis->get($seqARedisKey) ?? '[]');
            $previousContentB = json_decode($this->redis->get($seqBRedisKey) ?? '[]');
            $previousContentA[] = $currentPoint->column;
            $previousContentB[] = $currentPoint->row;
            $this->redis->set($seqARedisKey, json_encode($previousContentA));
            $this->redis->set($seqBRedisKey, json_encode($previousContentB));

            $currentPoint = $parentPoint;
        }
    }

    /**
     * @param list<float|int> $sequenceA
     * @param list<float|int> $sequenceB
     */
    private function setVariables(array $sequenceA, array $sequenceB): void
    {
        $this->sequenceA = $sequenceA;
        $this->sequenceB = $sequenceB;
        $this->sequenceALength = count($sequenceA);
        $this->sequenceBLength = count($sequenceB);
    }
}
