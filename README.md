# Zarovnávání DNA sekvencí převedených na časové řady
### Mikuláš Veselý (veselmik)

## Popis
Semestrální projekt byl pojat jako webová aplikace s grafickým uživatelským prostředím, kde je možno nahrát soubory s DNA sekvencemi, vybrat dvě porovnávané sekvence a získat jejich vzdálenost pomocí algoritmu DTW (Dynamic Time Warping). Dále je možno zkoumat i detailně namapování bodů jedné časové řady na druhou a naopak (pomocí vizualizace grafem).


## Způsob řešení

Jak již bylo zmíněno v předchozí sekci, samotné jádro problému je řešeno za pomocí DTW.  Pro pochopení tohoto algoritmu mi velmi pomohlo [YouTube video](https://www.youtube.com/watch?v=9GdbMc4CEhE&ab_channel=HermanKamper), které vizualizuje jeho průběh a získání celkové vzdálenosti. Ta se nachází ve vzdálenostní matici na úplném konci v rohu, kde index sloupců a řádků odpovídá poslednímu indexu jedné a druhé řady.
Zároveň je uchována i matice předků pro následný backtracking a získání detailního namapování bodů jedné řady na druhou.

## Implementace

Pro samotnou aplikaci jsem využil svých zkušeností s Dockerem a PHP. Aplikace běží v mnou připraveném kontejneru s PHP8.1, Redisem (pro cache výsledku) a Nginx (server). Výhodou kontejnerové aplikace je velice nízký vstupní nárok na softwarové vybavení stroje, neboť Docker i vše potřebné stáhne a vyinstaluje do kontejneru sám. Samotné spuštění (nebo čistá instalace) kontejneru je jednopříkazová (s pomocí Makefile).
S klasickými potřebami webové aplikace mi pomáhá framework Symfony ve verzi 6.0.
Pro vizuálně líbivé prostředí je ještě projekt doplněn CSS knihovnou Bootstrap 5 a o vykreslení grafů s časovými řadami se stará open-source grafová knihovna Chart.js.
## Experimentální sekce

Pro vyzkoušení jsem použil dva vzorové soubory CP0021055_30s_13s.ffn a CP0021055_10s_5s.ffn.

Pro váhy jednotlivých nukleobází jsem použíl hodnoty:

**A = 1 | C = 2 | G = 3 | T = 4**

Při spuštění s klasickou verzí DTW jsou metriky u prvního souboru:

| Soubor                | Čas počítání DTW | Cykly      |
|-----------------------|------------------|------------|
| CP0021055_30s_13s.ffn | ~35s             | 25,895,700 |
| CP0021055_10s_5s.ffn  | ~20s             | 14,895,900 |

Vzdalenosti u prvnich 3 nejblizsich DNA sekvenci

| Genom       | DTW vzdalenost |
|-------------|----------------|
| 1966-3063   | 467            |
| 3311-4435   | 491            |
 | 16502-17482 | 494            |


### Ořezání na diagonálu s šířkou 10 (na obě strany)

| Soubor                | Čas počítání DTW | Cykly   |
|-----------------------|------------------|---------|
| CP0021055_30s_13s.ffn | ~11s             | 727,820 |
| CP0021055_10s_5s.ffn  | ~5.5s            | 308,676 |

Vzdálenosti u prvnich 3 nejbližších DNA sekvencí

| Genom       | DTW vzdalenost | chyba |
|-------------|----------------|-------|
| 1966-3063   | 487            | 4,2 % |
| 3311-4435   | 489            | 0,5 % |
| 16502-17482 | 492            | 0,4 % |


### Ořezání na diagonálu s šířkou 3 (na obě strany)

| Soubor                | Čas počítání DTW | Cykly   |
|-----------------------|------------------|---------|
| CP0021055_30s_13s.ffn | ~11s             | 244,536 |
| CP0021055_10s_5s.ffn  | ~4.8s            | 103,558 |

Vzdálenosti u prvnich 3 nejbližších DNA sekvencí

| Genom       | DTW vzdalenost | chyba  |
|-------------|----------------|--------|
| 16502-17482 | 630            | 34,9 % |
| 1966-3063   | 641            | 37,3 % |
| 3311-4435   | 642            | 30,7 % |


## Vstup / Výstup

Vzorové soubory jsou ve složce dnaExampleFiles.

### Náhled GUI aplikace: 
![index](guiImages/index.png)

### Náhled obrazovky s nastavením parametrů aplikace:

![config](guiImages/config.png)

### Náhled obrazovky s daty o vzdálenostech všech genomů:

![config](guiImages/chart_1.png)

### Náhled obrazovky s detailním porovnáním časových řad dvou zvolených genomů:

![config](guiImages/chart_2.png)

## Diskuze

Pro samotné algoritmické jádro byl také použit jazyk PHP, což nemusí teoreticky může znamenat lehkou ztrátu rychlosti. I přesto, že je interpreter napsán v C a s nejnovější verzí 8.1 se opět velmi výkonově zlepšil, je to stále interpretovaný jazyk a je možné, že při napsání core funkcionality v C / C++ a jejím přidání ve formě PHP extension by výkon byl ještě o něco vyšší.
Dalšího zrychlení by se dalo docílit s pomocí použití aproximace, ovšem za cenu ztráty přesnosti ([https://cs.fit.edu/~pkc/papers/tdm04.pdf](https://cs.fit.edu/~pkc/papers/tdm04.pdf)).
Co se týká paměťové náročnosti, je možno se pohybovat v O(max(m, n)) a ne jen O(m*n), jako v případě mojí implementace ([https://en.wikipedia.org/wiki/Dynamic_time_warping#Complexity](https://en.wikipedia.org/wiki/Dynamic_time_warping#Complexity)).

## Závěr

Výsledkem je jednoduchá a přehledná webová aplikace s funkcí vizualizace DNA sekvencí jakožto datových řad a jejich vzdálenosti pomocí DTW. Při použití se vzorovými soubory z DNA databanky fungovala aplikace rozumně rychle a přesně.